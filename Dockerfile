FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
COPY . jenkins/
WORKDIR /jenkins
RUN dotnet restore JenkinsDemo.sln

FROM build AS publish
WORKDIR /jenkins/
RUN dotnet publish JenkinsDemo.sln -c Debug -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
COPY . jenkins/
WORKDIR /app
EXPOSE 8003

FROM runtime AS final
WORKDIR /app
COPY --from=publish /app .

ENTRYPOINT ["dotnet", "JenkinsDemo.dll"]
