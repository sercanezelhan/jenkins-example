﻿using Microsoft.AspNetCore.Mvc;

namespace JenkinsDemo.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class JenkinsController:ControllerBase
    {
        [HttpGet(nameof(Index))]
        public IActionResult Index()
        {
            return Ok("Jenkins V1");
        }
    }
}
